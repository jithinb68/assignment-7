var ctx = document.getElementById('myChart1');
var graphGradient = document.getElementById("myChart1").getContext('2d');
var saleGradientBg = graphGradient.createLinearGradient(25, 0, 25, 110);
saleGradientBg.addColorStop(0, '#ffffff');
saleGradientBg.addColorStop(1, '#ffc979');
var myChart1 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        datasets: [{
            data: [3, 5, 4, 5, 0, 5, 3, 4, 3, 5, 4, 5],
            borderColor: "#ffb400",
            borderWidth: 2,
            pointRadius: 0,
            backgroundColor: saleGradientBg,
            fill: true,
        }]

    },
    options: {
        elements: {
            line: {
                tension: 0
            }

        },
        scales: {
            xAxes: [{
                display: false
                    // gridLines: {
                    //     display: false,
                    //     drawBorder: false,

                // },
                // ticks: {
                //     display: false
                // }
            }],
            yAxes: [{
                display: false
                    // gridLines: {
                    //     display: false,
                    //     drawBorder: false,

                // },
                // ticks: {
                //     display: false
                // }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }
});


var ctx = document.getElementById('myChart2');
var myChart2 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25],
        datasets: [{
            data: [4, 6, 7, 6, 3, 2, 3, 4, 3, 3, 4, 4, 5, 5, 6, 4, 3, 2, 3, 4, 5, 4, 3, 3, 5],
            borderColor: "#0a4178",
            backgroundColor: '#0a4178',
            pointRadius: 0,

        }, {
            data: [9, 8, 9, 8, 7, 6, 8, 7, 5, 6, 7, 7, 8, 9, 10, 13, 11, 8, 9, 10, 11, 10, 12, 9, 10],
            borderColor: "#3b86d1",
            backgroundColor: '#3b86d1',
            pointRadius: 0,

        }, {
            data: [12, 12, 11, 13, 14, 15, 14, 12, 11, 12, 14, 15, 16, 17, 18, 16, 14, 12, 15, 17, 18, 17, 16, 17, 18],
            borderColor: "#d1ddeb",
            backgroundColor: '#d1ddeb',
            pointRadius: 0,

        }]

    },
    options: {
        scales: {
            xAxes: [{
                display: false
                    // gridLines: {
                    //     display: false,
                    //     drawBorder: false,

                // },
                // ticks: {
                //     display: false
                // }
            }],
            yAxes: [{
                display: false
                    // gridLines: {
                    //     display: false,
                    //     drawBorder: false,

                // },
                // ticks: {
                //     display: false
                // }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }
});

var ctx = document.getElementById('myChart3');

var graphGradient1 = document.getElementById("myChart3").getContext('2d');
var saleGradientBg1 = graphGradient1.createLinearGradient(45, 0, 5, 110);
saleGradientBg1.addColorStop(0, '#f8ecff');
saleGradientBg1.addColorStop(1, '#efd4fd');

var graphGradient2 = document.getElementById("myChart3").getContext('2d');
var saleGradientBg2 = graphGradient2.createLinearGradient(25, 0, 5, 110);
saleGradientBg2.addColorStop(0, '#fbffff');
saleGradientBg2.addColorStop(1, '#f6ffff');

var myChart3 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
        datasets: [{
            data: [50, 200, 50, 200, 100, 200, 50],
            borderColor: "#a43cda",
            backgroundColor: saleGradientBg1,
            pointRadius: 0,
            borderWidth: 2

        }, {
            data: [200, 330, 210, 220, 320, 250, 180],
            borderColor: "#00c8bf",
            backgroundColor: saleGradientBg2,
            pointRadius: 0,
            borderWidth: 2

        }]

    },
    options: {
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                    fontColor: "#001737",
                    min: 0,
                    max: 400,
                    stepSize: 100,
                    padding: 10,
                    fontSize: 11
                }
            }],
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    color: "rgba(151, 151, 151, 0.19)"
                },

                ticks: {
                    fontColor: "#001737",
                    min: 0,
                    max: 400,
                    stepSize: 100,
                    padding: 10,
                    fontSize: 11
                }
            }]
        },
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }
});

var ctx = document.getElementById('myChart4');
var myChart4 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24],
        datasets: [{
            data: [20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 10],
            backgroundColor: ["#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#3b86d1", "#b4d5f6", "#b4d5f6", "#b4d5f6", "#b4d5f6", "#b4d5f6", "#b4d5f6", "#b4d5f6"],

        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                barThickness: 6,
                display: false
            }],

        }
    }
});

var ctx = document.getElementById('myPieChart');
var myPieChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['Red', 'Blue', 'Yellow'],
        datasets: [{
            label: '# of Votes',
            data: [55, 25, 20],
            backgroundColor: [
                '#a43cda',
                '#21bf06',
                '#f39915',
            ],
            borderColor: [
                '#a43cda',
                '#21bf06',
                '#f39915',
            ],
            borderWidth: 1
        }]
    },
    options: {
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }

});

var ctx = document.getElementById('myDoughnutChart');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Red', 'Blue', 'Yellow'],
        datasets: [{
            label: '# of Votes',
            data: [53, 24, 23],
            backgroundColor: [
                '#a43cda',
                '#e6b8ff',
                '#d077ff',
            ],
            borderColor: [
                '#a43cda',
                '#e6b8ff',
                '#d077ff',
            ],
            borderWidth: 1
        }]
    },
    options: {
        cutoutPercentage: 70,
        legend: {
            display: false
        },
        tooltips: {
            enabled: false
        }
    }

});


var ctx = document.getElementById('myChart5');
// var myChart5 = new Chart(ctx).Bar(data, { barValueSpacing: 20 });
var myChart5 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [2014, 2015, 2016, 2017, 2018, 2019],
        datasets: [{
                data: [50, 40, 35, 45, 20, 50],
                backgroundColor: "#a43cda"
            },
            {
                data: [20, 45, 20, 50, 15, 40],
                backgroundColor: "#f39915"
            }
        ]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                // categoryPercentage: 0.1,
                // barPercentage: 0.1,
                gridLines: {
                    display: false,
                    drawBorder: false
                },
                ticks: {
                    fontColor: "#001737",
                    min: 0,
                    max: 60,
                    stepSize: 10,
                    fontSize: 11

                }

            }],
            xAxes: [{
                // categoryPercentage: 0.1,
                // barPercentage: 0.1,
                gridLines: {
                    display: false,
                    drawBorder: false
                },
                barThickness: 6,
                ticks: {
                    fontColor: "#001737",
                    padding: 8,
                    fontSize: 11

                }

            }],

        }
    }
});


var ctx = document.getElementById('myChart6');
var myChart6 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        datasets: [{
            data: [15, 17, 14, 15, 16, 17, 11, 13, 9, 14, 18, 20, 15, 15, 14, 13, 14, 18, 14, 13, 12, 12],
            backgroundColor: ["#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#a43cda", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2"],

        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                barThickness: 6,
                display: false
            }],

        }
    }
});

var ctx = document.getElementById('myChart7');
var myChart7 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        datasets: [{
            data: [15, 17, 14, 15, 16, 17, 11, 13, 9, 14, 18, 20, 15, 15, 14, 13, 14, 18, 14, 13, 12, 12],
            backgroundColor: ["#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#21bf06", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2"],

        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                barThickness: 6,
                display: false
            }],

        }
    }
});

var ctx = document.getElementById('myChart8');
var myChart8 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21],
        datasets: [{
            data: [15, 17, 14, 15, 16, 17, 11, 13, 9, 14, 18, 20, 15, 15, 14, 13, 14, 18, 14, 13, 12, 12],
            backgroundColor: ["#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#dc3545", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2", "#e1e1e2"],

        }]
    },
    options: {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                display: false
            }],
            xAxes: [{
                barThickness: 6,
                display: false
            }],

        }
    }
});